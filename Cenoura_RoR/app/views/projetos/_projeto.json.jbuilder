json.extract! projeto, :id, :project_name, :description, :begin, :end, :created_at, :updated_at
json.url projeto_url(projeto, format: :json)
