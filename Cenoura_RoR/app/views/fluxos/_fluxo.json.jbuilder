json.extract! fluxo, :id, :money, :description, :user, :day, :created_at, :updated_at
json.url fluxo_url(fluxo, format: :json)
