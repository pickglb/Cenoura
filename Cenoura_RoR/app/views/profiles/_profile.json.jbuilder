json.extract! profile, :id, :name, :occupation, :birth_date, :gender, :entry_date, :hire_date, :phone, :created_at, :updated_at
json.url profile_url(profile, format: :json)
