# frozen_string_literal: true
class User::RegistrationsController < Devise::RegistrationsController
  layout 'admin_lte_2_sign_up'
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
   #def new
    # super

  # end

  # POST /resource
  # def create
  #   super
  # end
  def create
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        #respond_with resource, location: after_sign_up_path_for(resource)
        redirect_to new_profile_path, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      #respond_with resource
      #redirect_to new_profile_path
    end

  end
  # GET /resource/edit
   def edit

      @user = User.find(params[:id])

      respond_to do |format|
        if @user.save && @user.update_attributes
          flash[:success] = "Cadastro atualizado."
          format.html { redirect_to home_index_path }
        else
          format.html { render 'edit'}
          format.json { render json: @user.errors }
        end
      end
   end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end
  #private
   # def user_params
      # NOTE: Using strong_parameters gem
      #params.require(:user).permit(:name, :birth_date, :gender ,:entry_date, :hire_date, :telegram, :phone)
    #end
  
  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

end
