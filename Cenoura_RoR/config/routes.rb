Rails.application.routes.draw do 
  resources :events
  resources :profiles
  resources :projetos
  resources :fluxos

  get 'home/index'
  get 'home_admin/index'
  devise_for :users, path: 'users', controllers: {sessions: "user/sessions", registrations: "user/registrations"}
  devise_for :admins, path: 'admins', controllers: {sessions: "admins/sessions", registrations: "admins/registrations"} 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # Dizer quais as pag alcançadas pelo pelo projeto
  get 'timeline/index'

  # root :to => 'home#index'

  devise_scope :admin do
    authenticated :admin do
       root :to => 'home#index', as: 'authenticated_admin'
       # get 'admins/sign_in'
       resources :projetos, only: [:show]
    end
    # unauthenticated :admin do
    #   root :to => 'user/sessions#new', as: 'unauthenticated_admin'
    # end
  end

  devise_scope :user do
    authenticated :user do
       root :to => 'home#index', as: 'authenticated_user'
       # get 'users/sign_in'
    end
    unauthenticated :user do
        root :to => 'user/sessions#new', as: 'unauthenticated_user'  
    end
  end 
end
