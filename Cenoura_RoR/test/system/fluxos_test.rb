require "application_system_test_case"

class FluxosTest < ApplicationSystemTestCase
  setup do
    @fluxo = fluxos(:one)
  end

  test "visiting the index" do
    visit fluxos_url
    assert_selector "h1", text: "Fluxos"
  end

  test "creating a Fluxo" do
    visit fluxos_url
    click_on "New Fluxo"

    fill_in "Day", with: @fluxo.day
    fill_in "Description", with: @fluxo.description
    fill_in "Money", with: @fluxo.money
    fill_in "User", with: @fluxo.user
    click_on "Create Fluxo"

    assert_text "Fluxo was successfully created"
    click_on "Back"
  end

  test "updating a Fluxo" do
    visit fluxos_url
    click_on "Edit", match: :first

    fill_in "Day", with: @fluxo.day
    fill_in "Description", with: @fluxo.description
    fill_in "Money", with: @fluxo.money
    fill_in "User", with: @fluxo.user
    click_on "Update Fluxo"

    assert_text "Fluxo was successfully updated"
    click_on "Back"
  end

  test "destroying a Fluxo" do
    visit fluxos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Fluxo was successfully destroyed"
  end
end
