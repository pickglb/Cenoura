class CreateFluxos < ActiveRecord::Migration[5.2]
  def change
    create_table :fluxos do |t|
      t.decimal :money
      t.string :description
      t.string :user
      t.date :day

      t.timestamps
    end
  end
end
