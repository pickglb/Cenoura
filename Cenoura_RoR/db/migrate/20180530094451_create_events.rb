class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.string :city
      t.date :begin_date
      t.date :end_date

      t.timestamps
    end
  end
end
