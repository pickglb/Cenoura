class CreateProjetos < ActiveRecord::Migration[5.2]
  def change
    create_table :projetos do |t|
      t.string :project_name
      t.string :description
      t.date :begin
      t.date :end

      t.timestamps
    end
  end
end
