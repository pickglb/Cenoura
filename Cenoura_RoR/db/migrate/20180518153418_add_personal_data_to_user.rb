class AddPersonalDataToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :birth_date, :date
    add_column :users, :gender, :string
    add_column :users, :entry_date, :date
    add_column :users, :hire_date, :date
    add_column :users, :phone, :string
    add_column :users, :telegram, :string
  end
end
