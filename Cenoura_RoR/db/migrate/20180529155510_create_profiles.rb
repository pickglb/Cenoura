class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :occupation
      t.date :birth_date
      t.string :gender
      t.date :entry_date
      t.date :hire_date
      t.string :phone

      t.timestamps
    end
  end
end
